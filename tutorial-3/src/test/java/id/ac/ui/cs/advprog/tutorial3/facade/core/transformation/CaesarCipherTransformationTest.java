package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CaesarCipherTransformationTest {
    private Class<?> caesarClass;

    @BeforeEach
    public void setup() throws Exception {
        caesarClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarCipherTransformation");
    }

    @Test
    public void testCaesarHasEncodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarEncodesCorrectly() throws Exception {
        String text = "abcdefgh";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "vwxyzabc";

        Spell result = new CaesarCipherTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarCipherTransformationEncodesCorrectlyWithCustomKey() throws Exception {
        String text = "abcdefg";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "efgabcd";

        Spell result = new AbyssalTransformation(3).encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarCipherTransformationHasDecodeMethod() throws Exception {
        Method translate = caesarClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testCaesarCipherTransformationDecodesCorrectly() throws Exception {
        String text = "abcdefg";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "fgabcde";

        Spell result = new AbyssalTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testCaesarCipherTransformationDecodesCorrectlyWithCustomKey() throws Exception {
        String text = "abcdefg";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "fgabcde";

        Spell result = new AbyssalTransformation(12).decode(spell);
        assertEquals(expected, result.getText());
    }
}
