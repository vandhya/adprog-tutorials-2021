package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {

    private Spellbook spellbook;
    private int charge;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        charge = 1;
    }

    @Override
    public String normalAttack() {
        charge = 1;
        return spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        if(charge == 1) {
            charge = 0;
            return spellbook.largeSpell();
        }else{
            charge = 1;
            return "Fail: Recharge required";
        }
    }

    @Override
    public String getName() {
        return spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return spellbook.getHolderName();
    }

}
