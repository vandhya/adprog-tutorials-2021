package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {

    // feel free to include more repositories if you think it might help :)
    private boolean firstTimeFind = true;

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    public void addAdapters(){
        for(Bow bow: bowRepository.findAll()){
            weaponRepository.save(new BowAdapter(bow));
        }
        for(Spellbook spellbook: spellbookRepository.findAll()){
            weaponRepository.save(new SpellbookAdapter(spellbook));
        }
    }

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        if (firstTimeFind) {
            addAdapters();
            firstTimeFind = false;
        }
        return weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if(attackType==0) {
            logRepository.addLog(String.format("%s attacked with %s (normal attack): %s",weapon.getHolderName(),weapon.getName(),weapon.normalAttack()));
        }else{
            logRepository.addLog(String.format("%s attacked with %s (charged attack): %s",weapon.getHolderName(),weapon.getName(),weapon.chargedAttack()));
        }
        weaponRepository.save(weapon);
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
