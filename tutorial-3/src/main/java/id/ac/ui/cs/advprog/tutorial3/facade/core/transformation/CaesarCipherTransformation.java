package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarCipherTransformation {
    private int key;

    public CaesarCipherTransformation(int key) {
        this.key = key;
    }

    public CaesarCipherTransformation() {
        this(21);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, Boolean encode) {
        String text = spell.getText();
        Codex codex = spell.getCodex();
        StringBuffer res = new StringBuffer();
        int selector = (encode) ? this.key : 26 - key;

        for (char c : text.toCharArray()) {
            if (Character.isLetter(c)) {
                char processedChar;
                if (Character.isUpperCase(c)) {
                    processedChar = (char) (((int) c + selector - 'A') % 26 + 'A');
                } else {
                    processedChar = (char) (((int) c + selector - 'a') % 26 + 'a');
                }
                res.append(processedChar);
            } else {
                res.append(c);
            }
        }

        return new Spell(res.toString(),codex);
    }
}
