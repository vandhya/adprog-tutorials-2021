package id.ac.ui.cs.advprog.tutorial3.facade.core.misc;

import com.sun.org.apache.bcel.internal.classfile.Code;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarCipherTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;

public class CodeProcessor {
    private AbyssalTransformation abyssalTransformation;
    private CelestialTransformation celestialTransformation;
    private CaesarCipherTransformation caesarCipherTransformation;

    public CodeProcessor() {
        abyssalTransformation = new AbyssalTransformation();
        celestialTransformation = new CelestialTransformation();
        caesarCipherTransformation = new CaesarCipherTransformation();
    }

    public String encode(String text){
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        Spell encode1 = celestialTransformation.encode(spell);
        Spell encode2 = abyssalTransformation.encode(encode1);
        Spell encode3 = caesarCipherTransformation.encode(encode2);
        Spell translated = CodexTranslator.translate(encode3, RunicCodex.getInstance());
        return translated.getText();
    }

    public String decode(String text){
        Spell encryptedSpell= new Spell(text,RunicCodex.getInstance());
        Spell translated = CodexTranslator.translate(encryptedSpell,AlphaCodex.getInstance());
        Spell decode1 = caesarCipherTransformation.decode(translated);
        Spell decode2 = abyssalTransformation.decode(decode1);
        Spell decode3 = celestialTransformation.decode(decode2);
        return decode3.getText();
    }
}
