- Satu mata kuliah dapat menerima banyak mahasiswa
- Mahasiswa hanya dapat menjadi asisten di satu mata kuliah
- Mahasiswa dapat mendaftar ke satu mata kuliah (anggap semua yang mendaftar ke suatu lowongan mata kuliah langsung 
  diterima)
- Mata kuliah yang sudah ada asisten tidak bisa dihapus
- Mahasiswa yang sudah menjadi asisten di suatu mata kuliah tidak bisa dihapus
---
- Setiap mahasiswa bekerja akan dicatat pada sebuah log
- Mahasiswa dapat membuat, memperbarui, dan menghapus log dan sistem akan menyimpannya
- asumsi log menyimpan waktu awal pekerjaan, waktu akhir pekerjaan, nama pekerjaan, jam kerja
---
- Mahasiswa dapat melihat laporan pembayaran untuk bulan tertentu
- Mengembalikan sebuah laporan yang berisikan semua pekerjaan asisten pada bulan tersebut dan sebuah summary jumlah 
  uang yang mereka dapatkan.
- Pada laporan tersebut diharapkan ada data bulan, jam kerja dengan satuan jam, pembayaran yang didapatkan pada 
  satuan Greil
- Pembayaran 350 Greil per jam. Sebuah log dapat saja berisikan data lebih singkat dari satu jam.
