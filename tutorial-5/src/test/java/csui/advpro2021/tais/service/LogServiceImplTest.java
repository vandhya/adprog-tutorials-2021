package csui.advpro2021.tais.service;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {

    @Mock
    private LogRepository logRepository;

    @Mock
    private MahasiswaRepository mahasiswaRepository;

    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private LogServiceImpl logService;

    @InjectMocks
    private MahasiswaServiceImpl mahasiswaService;

    @InjectMocks
    private MataKuliahServiceImpl mataKuliahService;

    private Log log;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        mataKuliah = new MataKuliah("adv","AdProg", "Ilkom");
        mahasiswa.setMataKuliah(mataKuliah);
        log = new Log();
        log.setStart(LocalDateTime.parse("2021-01-04T12:00:00"));
        log.setEndTime(LocalDateTime.parse("2021-01-04T15:00:00"));
        log.setDeskripsi("deskripsi");
        log.setMahasiswa(mahasiswa);
        log.setId(1L);
    }

}
