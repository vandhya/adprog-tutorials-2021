package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.LogServiceImpl;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.MataKuliahServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LogController.class)
public class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogServiceImpl logService;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private MataKuliahServiceImpl mataKuliahService;

    private Mahasiswa mahasiswa;

    private MataKuliah mataKuliah;

    private Log log;

    @BeforeEach
    public void setUp(){
        mahasiswa = new Mahasiswa("1906192052", "Maung Meong", "maung@cs.ui.ac.id", "4",
                "081317691718");
        mataKuliah = new MataKuliah("adv","AdProg", "Ilkom");
        mahasiswa.setMataKuliah(mataKuliah);
        log = new Log();
        log.setStart(LocalDateTime.parse("2021-01-04T12:00:00"));
        log.setEndTime(LocalDateTime.parse("2021-01-04T15:00:00"));
        log.setDeskripsi("deskripsi");
        log.setMahasiswa(mahasiswa);
        log.setId(1L);
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    private String mapLogToJson(Log log) throws JsonProcessingException {
        return String.format("{\"start\": \"2021-01-04T12:00:00\", \"endTime\": \"2021-01-04T15:00:00\", \"deskripsi\": \"%s\"}", log.getDeskripsi());
    }


    @Test
    public void testControllerPostLog() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.createLog(any(Log.class), any(String.class))).thenReturn(log);

        mvc.perform(post("/log/mahasiswa/1906192052")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapLogToJson(log)))
                .andExpect(jsonPath("$.deskripsi").value("deskripsi"));
    }

    @Test
    public void testControllerGetLogByNPM() throws Exception {
        Iterable<Log> logIterable = Arrays.asList(log);
        when(logService.getListLogByNPM(mahasiswa.getNpm())).thenReturn(logIterable);

        mvc.perform(get("/log/mahasiswa/1906192052")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value("1"));
    }

    @Test
    public void testControllerUpdateLog() throws Exception{

        logService.createLog(log, mahasiswa.getNpm());

        //Update log
        log.setDeskripsi("ok");

        when(logService.updateLog(any(Long.class),any(Log.class))).thenReturn(log);

        mvc.perform(put("/log/1").contentType(MediaType.APPLICATION_JSON)
                .content(mapLogToJson(log)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value("ok"));
    }

    @Test
    public void testControllerDeleteLog() throws Exception{
        logService.createLog(log, mahasiswa.getNpm());

        mvc.perform(delete("/log/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testControllerGetLaporanByNPM() throws Exception {
        Iterable<Laporan> laporanList = Arrays.asList(new Laporan("JANUARY",3.0));
        when(logService.getLaporanbyNPM(mahasiswa.getNpm())).thenReturn(laporanList);
        mvc.perform(get("/log/mahasiswa/laporan/1906192052").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].bulan").value("JANUARY"));
    }


}
