package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;

public interface LogService {
    Log createLog(Log log, String npm);

    Iterable<Log> getListLogByNPM(String npm);

    Log getLogById(Long id);

    Log updateLog(Long id, Log log);

    void deleteLogById(Long id);

    Iterable<Laporan> getLaporanbyNPM(String npm);

}
