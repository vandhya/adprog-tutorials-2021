package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Laporan;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(Log log, String npm){
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if(mahasiswa.getMataKuliah() != null) {
            log.setMahasiswa(mahasiswa);
            logRepository.save(log);
        }
        return log;
    }

    @Override
    public Iterable<Log> getListLogByNPM(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        return logRepository.findByMahasiswa(mahasiswa);
    }

    @Override
    public Log getLogById(Long id) {
        Log log = logRepository.findById(id);
        return log;
    }

    @Override
    public Log updateLog(Long id, Log log) {
        Log logToBeUpdated = logRepository.findById(id);
        log.setId(logToBeUpdated.getId());
        log.setMahasiswa(logToBeUpdated.getMahasiswa());
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogById(Long id) {
        logRepository.delete(getLogById(id));
    }

    @Override
    public Iterable<Laporan> getLaporanbyNPM(String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        // buat list log yang digabung berdasarkan bulan

        List<Log> logList = logRepository.findByMahasiswa(mahasiswa);
        List<Laporan> laporanList = new ArrayList<Laporan>();
        HashMap<String,Laporan> laporanHashMap = new HashMap<String,Laporan>();

        for(Log log : logList) {
            String bulan = log.getStart().getMonth().toString();
            double jamKerja = log.getDurasiKerja().toHours();
            Laporan laporanBaru = new Laporan(bulan,jamKerja);
            laporanList.add(laporanBaru);
        }

        // kalau bulan sudah ada, tinggal jumlah jam kerja + set pembayaran
        for(Laporan laporan : laporanList) {
            if (laporanHashMap.containsKey(laporan.getBulan())){
                laporanHashMap.get(laporan.getBulan()).addJamKerja(laporan.getJamKerja());
                laporanHashMap.get(laporan.getBulan()).setPembayaran();
            } else {
                laporanHashMap.put(laporan.getBulan(),laporan);
            }
        }

        return laporanHashMap.values();
    }

}
