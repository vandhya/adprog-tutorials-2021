package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "log")
public class LogController {
    @Autowired
    private LogService logService;

    @PostMapping(path = "/mahasiswa/{npm}",produces = {"application/json"})
    public ResponseEntity createLog(@PathVariable(value = "npm") String npm,
                                    @RequestBody Log log) {
        return ResponseEntity.ok(logService.createLog(log, npm));
    }

    @GetMapping(path = "/mahasiswa/{npm}", produces = {"application/json"})
    public ResponseEntity getLogByNpm(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getListLogByNPM(npm));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity updateLogById(@PathVariable(value = "id") String id,
                                        @RequestBody Log log) {
        return ResponseEntity.ok(logService.updateLog(Long.parseLong(id), log));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    public ResponseEntity deleteLog(@PathVariable(value = "id") String id) {
        logService.deleteLogById(Long.parseLong(id));
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @GetMapping(path = "/mahasiswa/laporan/{npm}", produces = {"application/json"})
    public ResponseEntity getLaporanByNPM(@PathVariable(value = "npm") String npm) {
        return ResponseEntity.ok(logService.getLaporanbyNPM(npm));
    }
}
