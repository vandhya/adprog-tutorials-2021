package csui.advpro2021.tais.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Laporan {

    private String bulan;

    private double jamKerja = 0;

    private long pembayaran = 0;

    public void setPembayaran(){
        this.pembayaran = (long) (jamKerja * 350);
    }

    public void addJamKerja(double jamKerja) {
        this.jamKerja = this.jamKerja + jamKerja;
    }

    public Laporan(String bulan, double jamKerja){
        this.bulan = bulan;
        this.jamKerja = jamKerja;
        this.pembayaran = (long) (jamKerja * 350);
    }
}

