package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {

    @Id
    @Column(name = "id", updatable = false, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "waktu_mulai")
    private LocalDateTime start;

    @Column(name = "waktu_selesai")
    private LocalDateTime endTime;

    @Column(name="durasi_kerja")
    private Duration durasiKerja;

    @ManyToOne
    @JoinColumn(name = "npm")
    private Mahasiswa mahasiswa;

    @JsonCreator
    public Log(
            @JsonProperty("deskripsi") String deskripsi,
            @JsonProperty("start") String start,
            @JsonProperty("endTime") String endTime
    ) {
        this.deskripsi = deskripsi;
        this.start = LocalDateTime.parse(start);
        this.endTime = LocalDateTime.parse(endTime);
        this.durasiKerja = Duration.between(this.start,this.endTime);
    }
}
