package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    @Override
    public String attack() {
        return "I GOT A GUN AND I'M NOT AFRAID TO USE IT";
    }

    @Override
    public String getType() {
        return "Gun Attack";
    }
}
