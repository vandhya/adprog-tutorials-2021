package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    @Override
    public String attack() {
        return "U WANNA GET SHANKED M8?";
    }

    @Override
    public String getType() {
        return "Sword Attack";
    }
}
