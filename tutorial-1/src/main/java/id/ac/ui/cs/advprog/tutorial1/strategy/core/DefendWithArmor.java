package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    @Override
    public String defend() {
        return "call the ambulance.. BUT NOT FOR ME";
    }

    @Override
    public String getType() {
        return "Armor Defend";
    }
}
