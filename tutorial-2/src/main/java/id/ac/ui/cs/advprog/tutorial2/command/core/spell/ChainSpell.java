package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spells;
    // initiate list of spells
    public ChainSpell(ArrayList<Spell> spells){
        this.spells = spells;
    }

    @Override // invokes every spell in spells list
    public void cast() {
        for (Spell spell:spells) {
            spell.cast();
        }
    }

    @Override
    public void undo() { // undo every spell from last index
        for (int i = spells.size() - 1; i >= 0 ; i--) {
            spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
