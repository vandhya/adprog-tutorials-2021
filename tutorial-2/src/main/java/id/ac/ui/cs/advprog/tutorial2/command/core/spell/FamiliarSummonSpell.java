package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;

public class FamiliarSummonSpell extends FamiliarSpell {
	// initiates Familiar spirit through FamiliarSpell
    public FamiliarSummonSpell(Familiar familiar){
        super(familiar);
    }

    @Override
    public void cast() {
        this.familiar.summon();
    } // invokes familiar to cast summon

    @Override
    public String spellName() {
        return familiar.getRace() + ":Summon";
    }
}
