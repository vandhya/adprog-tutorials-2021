package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import java.lang.Thread;

public class OrderDrink {

    private String drink;

    private volatile static OrderDrink orderDrink = null;

    private OrderDrink() {
        try{
            System.out.println("Creating.....");
            Thread.sleep(2000);
            System.out.println("Done.....");
        } catch(InterruptedException e){
            e.printStackTrace();
        }

    }

    // lazy instantiation approach
    public static OrderDrink getInstance() {
        if(orderDrink==null){
            synchronized (OrderDrink.class) {
                if(orderDrink==null) {
                    orderDrink = new OrderDrink();
                }
            }
        }
        return orderDrink;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    @Override
    public String toString() {
        return drink == null ? "noName" : drink;
    }
}
