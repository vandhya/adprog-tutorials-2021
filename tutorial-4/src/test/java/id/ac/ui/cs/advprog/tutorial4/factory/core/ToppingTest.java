package id.ac.ui.cs.advprog.tutorial4.factory.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import org.junit.jupiter.api.Test;

public class ToppingTest {

    @Test
    public void testToppingsShouldReturnCorrectDescriptions(){
        BoiledEgg boiledEgg = new BoiledEgg();
        Cheese cheese = new Cheese();
        Flower flower = new Flower();
        Mushroom mushroom = new Mushroom();

        assertEquals("Adding Guahuan Boiled Egg Topping",boiledEgg.getDescription());
        assertEquals("Adding Shredded Cheese Topping...",cheese.getDescription());
        assertEquals("Adding Xinqin Flower Topping...",flower.getDescription());
        assertEquals("Adding Shiitake Mushroom Topping...",mushroom.getDescription());
    }
}
