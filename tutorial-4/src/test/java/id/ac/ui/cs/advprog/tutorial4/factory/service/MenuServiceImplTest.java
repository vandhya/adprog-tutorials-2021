package id.ac.ui.cs.advprog.tutorial4.factory.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class MenuServiceImplTest {

    @Test
    public void testWhenMenuServiceIsInstantiatedShouldReturnSaveMenus(){
        MenuServiceImpl menuService = new MenuServiceImpl();
        assertFalse(menuService.getMenus().isEmpty());
    }
}
