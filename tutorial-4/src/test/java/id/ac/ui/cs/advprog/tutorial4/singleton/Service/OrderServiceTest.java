package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class OrderServiceTest {

    private OrderServiceImpl orderService;

    @BeforeEach
    public void setUp(){
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testGetDrinkShouldReturnOrderDrinkInstance(){
        orderService.orderADrink("ok");
        assertNotNull(orderService.getDrink());
    }

    @Test
    public void testGetFoodShouldReturnOrderFoodInstance(){
        orderService.orderAFood("ok");
        assertNotNull(orderService.getFood());
    }

}
