package id.ac.ui.cs.advprog.tutorial4.factory.core;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MenusTest {
    private Menu menu;

    @BeforeEach
    public void setUp(){
        menu = new Menu("menu", new Shirataki(), new Pork(), new BoiledEgg(), new Spicy());
    }

    @Test
    public void testMenuShouldReturnCorrectAttributes(){

        assertEquals("menu", menu.getName());
        assertNotNull(menu.getNoodle());
        assertNotNull(menu.getMeat());
        assertNotNull(menu.getTopping());
        assertNotNull(menu.getFlavor());

        InuzumaRamen inuzumaRamen = new InuzumaRamen("menra");
        LiyuanSoba liyuanSoba = new LiyuanSoba("baso");
        MondoUdon mondoUdon = new MondoUdon("donu");
        SnevnezhaShirataki snevnezhaShirataki = new SnevnezhaShirataki("takishira");
        assertNotNull(inuzumaRamen);
        assertNotNull(liyuanSoba);
        assertNotNull(mondoUdon);
        assertNotNull(snevnezhaShirataki);

    }

}
