package id.ac.ui.cs.advprog.tutorial4.factory.core;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MeatTest {

    @Test
    public void testMeatsShouldReturnCorrectDescriptions(){
        Beef beef = new Beef();
        Chicken chicken = new Chicken();
        Fish fish = new Fish();
        Pork pork = new Pork();

        assertEquals("Adding Maro Beef Meat...",beef.getDescription());
        assertEquals("Adding Wintervale Chicken Meat...",chicken.getDescription());
        assertEquals("Adding Zhangyun Salmon Fish Meat...",fish.getDescription());
        assertEquals("Adding Tian Xu Pork Meat...",pork.getDescription());

    }
}
