package id.ac.ui.cs.advprog.tutorial4.factory.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import org.junit.jupiter.api.Test;

public class NoodeTest {

    @Test
    public void testNoodlesShouldReturnCorrectDescription(){
        Ramen ramen = new Ramen();
        Shirataki shirataki = new Shirataki();
        Soba soba = new Soba();
        Udon udon = new Udon();

        assertEquals("Adding Inuzuma Ramen Noodles...",ramen.getDescription());
        assertEquals("Adding Snevnezha Shirataki Noodles...",shirataki.getDescription());
        assertEquals("Adding Liyuan Soba Noodles...",soba.getDescription());
        assertEquals("Adding Mondo Udon Noodles...",udon.getDescription());

    }
}
