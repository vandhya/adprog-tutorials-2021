package id.ac.ui.cs.advprog.tutorial4.singleton.Core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import org.junit.jupiter.api.Test;

public class SingletonOrderDrinkTest {

    @Test
    public void testCallingGetInstanceTwiceShouldReturnSameInstance() {
        OrderDrink orderDrink1 = OrderDrink.getInstance();
        OrderDrink orderDrink2 = OrderDrink.getInstance();
        assertEquals(orderDrink1,orderDrink2);
    }

    @Test
    public void testSetAndGetDrinkMethod() {
        OrderDrink orderDrink = OrderDrink.getInstance();
        orderDrink.setDrink("ok");
        assertEquals("ok",orderDrink.getDrink());
    }
}
