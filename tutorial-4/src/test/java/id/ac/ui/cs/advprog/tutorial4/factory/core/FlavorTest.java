package id.ac.ui.cs.advprog.tutorial4.factory.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import org.junit.jupiter.api.Test;

public class FlavorTest {

    @Test
    public void testFlavorsShouldReturnCorrectDescriptions(){
        Salty salty = new Salty();
        Spicy spicy = new Spicy();
        Sweet sweet = new Sweet();
        Umami umami = new Umami();

        assertEquals("Adding a pinch of salt...",salty.getDescription());
        assertEquals("Adding Liyuan Chili Powder...",spicy.getDescription());
        assertEquals("Adding a dash of Sweet Soy Sauce...",sweet.getDescription());
        assertEquals("Adding WanPlus Specialty MSG flavoring...",umami.getDescription());
    }
}
