package id.ac.ui.cs.advprog.tutorial4.singleton.Core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SingletonOrderFoodTest {

    private OrderFood orderFood;

    @BeforeEach
    public void setUp() throws Exception {
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testCallingGetInstanceTwiceShouldReturnSameInstance() {
        OrderFood orderFood1 = OrderFood.getInstance();
        OrderFood orderFood2 = OrderFood.getInstance();
        assertEquals(orderFood1,orderFood2);
    }

    @Test
    public void testSetAndGetFoodMethod() {
        orderFood.setFood("ok");
        assertEquals("ok",orderFood.getFood());
    }

    @Test
    public void testToStringAfterSettingNameShouldReturnFood(){
        orderFood.setFood("ok");
        assertEquals("ok",orderFood.toString());
    }


}
