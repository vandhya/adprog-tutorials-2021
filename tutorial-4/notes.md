Lazy approach VS Eager approach
---

Pada lazy instantiation approach, kita baru membuat sebuah
instance dari suatu class Singleton saat kita memanggil
`getInstance()` dari kelas tersebut. Kita cek terlebih
dahulu apakah instance dari singleton class sudah pernah
dibuat atau belum. <br>
Jika belum, kita membuat instance dari singleton 
melalui constructor private dirinya dan assign
variabel unik yang merupakan instance dirinya sendiri.
Dalam lazy approach, jika kita tidak memerlukan instance
dari singleton, maka tidak akan dibuat. Jika
sudah ada instance dari singleton class, maka kita
hanya perlu me-return instance unik yang sudah dibuat.<br>
Kelebihan dari lazy approach: Jika kita tidak membutuhkan 
instance dari singleton class, maka tidak perlu dibuat. <br>
Kelemahan dari lazy approach: Terdapat masalah dengan multithreading,
di mana JVM bisa menginstantiate lebih dari satu instance dari
singleton class. Hal ini dapat diselesaikan dengan menggunakan
synchronized method, atau dengan double-checked locking.



Sementara dalam eager approach, aplikasi yang kita buat 
selalu akan meng-instantiate satu instance dari singleton class, 
melalui static initializer sehingga saat kita memanggil getInstance() 
dari class singleton, kita hanya perlu me-return instance yang 
sudah dibuat tersebut. JVM membuat instance unik saat class di load.<br>
Kelebihan dari eager approach: Tidak perlu pusing dengan multi-threading,
pasti thread-safe. JVM menjamin bahwa instance akan dibuat sebelum thread 
mengakses static variable uniqueInstance. 
Kelemahan dari eagar approach: Karena object singleton pasti akan di-
instantiate, jika kita akhirnya tidak membutuhkan singleton class tersebut,
maka object yang dibuat tidak akan digunakan. Selain itu, kemungkinan 
kurang jelas dengan developer yang belum familiar dengan pattern standar.